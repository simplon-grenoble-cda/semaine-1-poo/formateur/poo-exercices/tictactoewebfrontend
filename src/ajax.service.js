import axios from 'axios';
import assert from 'power-assert';

export const AjaxService = {
  async getGameId() {
    return axios.get('/games/new');
  },
  async getGameState({gameId}) {
    return axios.get(`/games/${gameId}`);
  };
  async playMove({gameId, playerId, cellId}) {
    /* sanity checks */
    assert(['A', 'B'].includes(playerId));
    assert(typeof cellId === 'number');
    assert(cellId >= 0 && cellId <= 8);

    return axios.post(`/games/${gameId}`, {
      player: playerId,
      cell: cellId
    });
  }
}

import axios from 'axios';
import VueAxios from 'vue-axios';
import { API_URL } from './config';

export const ApiService = {
  init(vueApp) {
    vueApp.use(VueAxios, axios);
    vueApp.axios.defaults.baseURL = API_URL;
  }
};

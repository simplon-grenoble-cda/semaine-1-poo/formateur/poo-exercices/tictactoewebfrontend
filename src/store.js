import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    currentGameValid: false,
    grid: [],
    gameId: null,
    nextRound: null,
    winner: null,
    loading: false
  },
  getters: {
    getMyProperty: state => {
      return state.my_property;
    }
  },
  mutations: {
    resetGameState: (state) => {
      state.currentGameValid = false;
      state.gameId = null;
      state.grid = [];
      state.nextRound = null;
      state.winner = null;
    },
    setGameState: (state, gameState) => {
      state.currentGameValid = true;
      state.gameId = gameState.gameId;
      state.grid: gameState.grid;
      state.nextRound = gameState.nextRound;
      state.winner = gameState.winner;
    },
    setLoading: (state, loading) => {
      state.loading = loading;
    }
  },
  actions: {
    initState: (context) => {
      /* Get gameID from local storage if exists */
      let gameId = localStorage.getItem('gameId');
      /* Get game current state */
      if (gameId) {
        context.dispatch('fetchGameUpdate', {gameId});
      }
    },
    removeCurrentGame: (context) => {
      /* delete localStorage */
      localStorage.deleteItem('gameId');
      /* update state */
      context.commit('resetGameState');
    },
    playMove: async (context, {cellId}) => {
      let {date: gameState} = await AjaxService.playMove({
        gameId: context.state.gameId,
        playerId: context.state.nextRound,
        cellId
      });
      context.commit('setGameState', gameState);
    },
    fetchGameUpdate: async (context, {gameId}) => {
      let id = gameId || context.state.gameId;
      if (!id) {
        throw new Error('fetchGameUpdate() called without gameId');
      }
      try {
        let {data: gameState} = await AjaxService.getGameState({gameId: id});
        context.commit('setGameState', gameState);
      } catch (e) {
        context.dispatch('removeCurrentGame');
      }
    },
    getNewGame: async (context) => {
      let {data: {gameId}} = await AjaxService.getNewGame();
      context.dispatch('fetchGameUpdate', {gameId});
    }
  }
});

import { createApp } from 'vue'
import { ApiService } from './api.service';

import App from './App.vue';

let vueApp = createApp(App);
ApiService.init(vueApp);
vueApp.mount('#app');
